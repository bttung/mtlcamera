package jp.co.cyberagent.mtl.camera;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends Activity{
    public static final String IMAGE_NAME = "/sdcard/mtl_camera_test.bmp";
    private CameraPreview mPreview;
    private RelativeLayout mLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLayout = new RelativeLayout(this);
        setContentView(mLayout);
    }

    /** カメラが現在使用可能かどうか */
    private boolean mCameraAvailable = true;

    @Override
    protected void onResume() {
        super.onResume();
        mPreview = new CameraPreview(this, 0, CameraPreview.LayoutMode.FitToParent);
        LayoutParams previewLayoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);


        mPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCameraAvailable) { // 写真の生成中は撮影しない
                    mPreview.mCamera.takePicture(null, null, null, pictureCallback);
                    mCameraAvailable = false;
                }
            }
        });

        mLayout.addView(mPreview, 0, previewLayoutParams);
        Toast.makeText(this, "タップして撮影", Toast.LENGTH_SHORT).show();
    }

    /**
     * 写真を撮ったときに呼ばれるコールバック関数
     */
    private final Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            // SDカードにJPEGデータを保存する
            if (data != null) {
                FileOutputStream fileOutputStream = null;

                try {
                    fileOutputStream = new FileOutputStream(IMAGE_NAME);
                    fileOutputStream.write(data);
                    fileOutputStream.close();

//                    Toast.makeText(MainActivity.this,
//                        "画像を保存しました", Toast.LENGTH_LONG).show();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                camera.startPreview();
                mCameraAvailable = true;

                // Go to Image Edit Actitivy
                Intent intent = new Intent(MainActivity.this, ImageEffectActivity.class);
                startActivity(intent);
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        mPreview.stop();
        mLayout.removeView(mPreview);
        mPreview = null;
    }

}
